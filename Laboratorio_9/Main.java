import java.util.Scanner;

class Main {
  public static void main(String[] args) {
     Scanner  as = new Scanner(System.in);
       boolean salir = false;
      int opcion; //Guardaremos la opcion del usuario
        
       while(!salir){
            
           System.out.println("1. Opcion 1");
           System.out.println("2. Opcion 2");
           System.out.println("3. Opcion 3");
           System.out.println("4. Salir");
            
           System.out.println("Escribe una de las opciones");
           opcion = as.nextInt();
            
           switch(opcion){
               case 1:
                   System.out.println("Has seleccionado la opcion 1");
                   iniciarSesion();
                   break;
               case 2:
                   System.out.println("Has seleccionado la opcion 2");
                   Administrador();
                   break;
                case 3:
                   System.out.println("Has seleccionado la opcion 3");
                   Oficial();
                   break;
                case 4:
                   salir=true;
                   break;
                default:
                   System.out.println("Solo números entre 1 y 4");
           }
            
       }
        
  }
}