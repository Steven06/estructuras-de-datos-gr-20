import java.util.Date;

public class Cuenta{
  private int id;
  private double balance;
  private double tasadeinteresanual;
  private Date fechaDeCreacion;

  public Cuenta(){
    id = 0;
    balance = 0;
    tasadeinteresanual = 0;
  }

  public Cuenta(int id, double balance, double tasadeinteresanual){
    this.id = id;
    this.balance = balance;
    this.tasadeinteresanual = tasadeinteresanual;
  }

  public int getid(){
    return this.id;
  }

  public void setid(int id){
    this.id = id;
  }

  public double getbalance(){
    return this.balance;
  }

  public void setbalance(double balance){
    this.balance = balance;
  }

  public double gettasaDeInteresAnual(){
    return this.tasaDeInteresAnual;
  }

  public void settasaDeInteresAnual(double tasadeinteresanual){
    this.tasaDeInteresAnual = tasaDeInteresAnual;
  }

  public Date getfechaDeCreacion(){
    Date fechaDeCreacion = new Date();
    this.
  }

  public void setfechaDeCreacion(Date fechaDeCreacion){
    this.fechaDeCreacion = fechaDeCreacion;
  }

  public double obtenerTasaDeInteresMensual(){
    double tasaDeInteresMensual = tasadeinteresanual /12;
    return tasaDeInteresMensual;
  }

  public double calcularInteresMensual(){
    double calcularInteresMensual = balance * obtenerTasaDeInteresMensual;
    return calcularInteresMensual;
  }

  public void retirarDinero(double retirarDinero){
    balance = balance - retirarDinero;
  }

  public void depositarDinero(double depositarDinero){
    balance = balance + depositarDinero;
  }
}

