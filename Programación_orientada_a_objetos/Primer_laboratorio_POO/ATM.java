import java.util.InputMismatchException;
import java.util.Scanner;

public class ATM{
  public ATM(){
    private Cuenta[] arregloCuentas = new Cuenta[10];
    for(int indice = 0; indice < arregloCuentas.length; indice++){ 
    arreglo[indice] = new Cuenta(indice,100000);
    }
  int opcion; //Guardaremos la opcion del usuario
  boolean salir = false;
  boolean ciclo = true;
  double retirarDinero1;
  double depositarDinero1;
  	while(ciclo = true) {
    System.out.println("Ingrese su id:"); //solicitar el id
    Scanner sn = new Scanner(System.in); //recibir opcióón del usario
    int id = sn.nextInt();
         
      salir = false;
      while(!salir && id>=0 && id<10){
          System.out.println("Menú principal");
          System.out.println("1. Ver el balance actual");
          System.out.println("2. Retirar dinero");
          System.out.println("3. Depositar dinero");
          System.out.println("4. Salir");
      try{
      System.out.println("Ingrese su selección"); //elegir una opcióón
      opcion = sn.nextInt(); //guardar opción
    
      switch(opcion){
        case 1:
            double balance1 = arreglo[id].getbalance();
            System.out.println("Balance Actual:" + balance1);
            break;
        
        case 2:
            System.out.println("Ingrese el monto que desea retirar");
            retirarDinero1 = sn.nextDouble();
            arreglo[id].retirarDinero(retirarDinero1);
            break;    
        case 3:
            System.out.println("Ingrese el monto que desea depositar");
            depositarDinero1 = sn.nextDouble();
            arreglo[id].depositarDinero(depositarDinero1);
            break;        
        
        case 4:
            salir = true;
            break;
        default:
            System.out.println("Las opciones son entre 1 y 4");
      
      }
    }catch(InputMismatchException e){ //verificar si es un entero o no
      System.out.println("Debes introducir un número");
      sn.next();
    }
  }
  }
  }
}
