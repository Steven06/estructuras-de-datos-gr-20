//Steven Alvarado Aguilar
//2019044923
import java.util.Scanner;
import static java.lang.System.out;

public​ class​ Main{
  public static void main(String[] args){
    Pila_TEC pila = new Pila_TEC(); //Instanciamos la pila de la clase Pila_TEC.
    Scanner numero = new Scanner(System.in); //Lee valores de entrada que se entran por el teclado.
    int numero_ingresado; //Se crea un entero para ir guardando el número en cada recursión al add del push.

    int indice; //Entero llamado indice lleva la cantidad del indice de números ingresados.
    for(indice = 0; indice < 5; indice ++){ // Se hace un for de que el indice debe de llegar máximo a 5 (desde 0 a 4).
      System.out.print("Ingrese un número: ");
      numero_ingresado = numero.nextInt(); //El número ingresado va ser igual al siguiente número de entrada del scanner pero
	//en otro push.
      pila.push(numero_ingresado); // Acá se guarda el numro ingresado en un push diferente.
      System.out.println("");
    }
        
    System.out.println("Lista alrevez");
        
    pila.mostrar_lista(); // Acá se llama a una clase en Pila_TEC que imprime la lista alrévez.
  }
}

//Información del vídeo https://youtu.be/zPJ5KhfrcyQ
