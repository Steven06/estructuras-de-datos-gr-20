//Steven Alvarado Aguilar
//2019044923
import​ java.util.ArrayList;

public​ class​ Pila_TEC extends ArrayList{
  
  public Pila_TEC(){
    super();
  }
    
  public​ boolean​ isEmpty(){
    return​ super.isEmpty();
  }

  public​ int​ getSize(){
    return​ super.size();
  }

  public​ Object peek(){
    return​ super.get(getSize() - 1);
  }

  public​ Object pop(){
    Object o = super.get(getSize() - 1); super.remove(getSize() - 1);
    return​ o;
  }

  public​ void​ push(Object o){
    super.add(o);
  }

  @Override
  public​ String toString(){
    return​ "pila: " + super.toString();
  } 

  public​ void mostrar_lista(){ //Aca Mostramos la lista de forma alrévez. 
    int cantidad = getSize(); //El entero cantidad va ser lo  que contenga getSize. 
    for(int indice = 4; indice >= 0; indice--){ //Aquí es donde hacemos un for que inicie en la última posición y después se vaya diminuyendo su indice hasta llevar a cero para después imprimirlo del último al primero en la linea de abajo.
      System.out.print(pop() + "  "); //Me permite ir imprimiendo cada número y mostrarlo en pantalla.
    }
  }
}
