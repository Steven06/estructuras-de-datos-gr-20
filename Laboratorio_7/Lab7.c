#include<stdio.h>
#include<string.h>
#include<stdlib.h>

struct nodo{
    char nombre;//nombre del vertice o nodo
    struct nodo *sgte;
    struct arista *ady;//puntero hacia la primera arista del nodo
};

struct arista{
    struct nodo *destino;//puntero al nodo de llegada
    struct arista *sgte;
};

typedef struct nodo *Tnodo;//  Tipo Nodo
typedef struct arista *Tarista; //Tipo Arista
 
struct nodo * crearnododato(int dato);

Tnodo p;//puntero cabeza
 
void menu();
void insertar_nodo();
void agrega_arista(Tnodo, Tnodo, Tarista);
void insertar_arista();
void vaciar_aristas(Tnodo);
void eliminar_nodo();
void eliminar_arista();
void mostrar_grafo();
void mostrar_aristas();
 
 
/*                        Funcion Principal
---------------------------------------------------------------------*/
int main(void){   
    p=NULL;
    int opcion;     // opcion del menu         
    int ciclo =1;
	while(ciclo){
		printf("\n---------------------------------------------------------------------");
		printf("\n(1) Insertar un nodo");
		printf("\n(2) Insertar una arista");
		printf("\n(3) Eliminar un nodo");
		printf("\n(4) Eliminar una arista");
		printf("\n(5) Mostrar grafo");
		printf("\n(6) Mostart aristas de un nodo");
		printf("\n(7) Salir");
		printf("\n---------------------------------------------------------------------");
		printf("\n>> Opcion deseada: ");
		scanf("%d",&opcion);
		switch(opcion){
			case 1:
				insertar_nodo();
				break;

			case 2:
				insertar_arista();
				break;
			case 3:
				eliminar_nodo();
				break;

			case 4:
				eliminar_arista();
				break;
			case 5:
                mostrar_grafo();
				break;
			case 6:
				mostrar_aristas();
				break;

			case 7:
			    break;
			default: ciclo = 0;
		}
	}
}
   
/*                      INSERTAR NODO AL GRAFO
---------------------------------------------------------------------*/
void insertar_nodo(){
    struct nodo * crearnododato(int dato)
    nodo *nuevo = malloc(sizeof(nodo);
    printf("INGRESE VARIABLE:");
    nuevo->nombre = NULL;
    nuevo->sgte = NULL;
    nuevo->ady=NULL;
 
    if(p==NULL)
     {
        p = nuevo;
        printf("PRIMER NODO...!!!");
      }
    else
     {
        t = p;
        while(t->sgte!=NULL)
         {
            t = t->sgte;
          }
        t->sgte = nuevo;
        printf("NODO INGRESADO...!!!");
      }
 
 }
 
/*                      AGREGAR ARISTA
    funcion que utilizada para agregar la arista a dos nodos
---------------------------------------------------------------------*/
void agrega_arista(Tnodo aux, Tnodo aux2, Tarista nuevo)
{
    Tarista q;
    if(aux->ady==NULL)
    {   aux->ady=nuevo;
        nuevo->destino=aux2;
        printf("PRIMERA ARISTA....!");
    }
    else
    {   q=aux->ady;
        while(q->sgte!=NULL)
        q=q->sgte;
        nuevo->destino=aux2;
        q->sgte=nuevo;
        printf("ARISTA AGREGADA...!!!!");
    }
 
}
/*                      INSERTAR ARISTA
    funcion que busca las posiciones de memoria de los nodos
    y hace llamado a agregar_arista para insertar la arista
---------------------------------------------------------------------*/
void insertar_arista()
{   char ini, fin;
    Tarista *nuevo= malloc(sizeof(Tarista);
    Tnodo aux, aux2;
 
    if(p==NULL)
     {
        printf("GRAFO VACIO...!!!!");
        return;
     }
    nuevo->sgte=NULL;
    printf("INGRESE NODO DE INICIO:");
    scanf("%d",ini);
    printf("INGRESE NODO FINAL:");
    scanf("%d", fin);
    aux=p;
    aux2=p;
    while(aux2!=NULL)
    {
        if(aux2->nombre==fin)
        {
            break;
        }
 
        aux2=aux2->sgte;
    }
    while(aux!=NULL)
    {
        if(aux->nombre==ini)
        {
            agrega_arista(aux,aux2, nuevo);
            return;
        }
 
        aux = aux->sgte;
    }
}
 
/*          FUNCION PARA BORRAR TODAS LAS ARISTAS DE UN NODO
    esta funcion es utilizada al borrar un nodo pues si tiene aristas
    es nesesario borrarlas tambien y dejar libre la memoria
---------------------------------------------------------------------*/
void vaciar_aristas(Tnodo aux)
{
    Tarista q,r;
    q=aux->ady;
    while(q->sgte!=NULL)
    {
        r=q;
        q=q->sgte;
        delete(r);
    }
}
/*                      ELIMINAR NODO
    funcion utilizada para eliminar un nodo del grafo
    pero para eso tambien tiene que eliminar sus aristas por lo cual
    llama a la funcion vaciar_aristas para borrarlas
---------------------------------------------------------------------*/
void eliminar_nodo()
{   char var;
    Tnodo aux,ant;
    aux=p;
    printf("ELIMINAR UN NODO\n");
    if(p==NULL){
        printf("GRAFO VACIO...!!!!");
        return;
    }
    printf("INGRESE NOMBRE DE VARIABLE:");
    scanf("%d", &var);
 
    while(aux!=NULL)
    {
        if(aux->nombre==var)
        {
            if(aux->ady!=NULL)
                vaciar_aristas(aux);
 
            if(aux==p)
            {
 
                    p=p->sgte;
                    delete(aux);
                    printf("NODO ELIMINADO...!!!!");
                    return;
 
 
 
            }
            else
            {
                ant->sgte = aux->sgte;
                delete(aux);
                printf("NODO ELIMINADO...!!!!");
                return;
            }
        }
        else
        {
            ant=aux;
            aux=aux->sgte;
         }
    }
 
}
 
/*                      ELIMINAR ARISTA
    funcion utilizada para eliminar una arista
---------------------------------------------------------------------*/
void eliminar_arista()
{
char ini, fin;
    Tnodo aux, aux2;
    Tarista q,r;
    printf("\n ELIMINAR ARISTA\n");
    printf("INGRESE NODO DE INICIO:");
    scanf("%d", ini);
    printf("INGRESE NODO FINAL:");
    scanf("%d", fin);
    aux=p;
    aux2=p;
    while(aux2!=NULL)
    {
        if(aux2->nombre==fin)
        {
            break;
        }
        else
        aux2=aux2->sgte;
    }
     while(aux!=NULL)
    {
        if(aux->nombre==ini)
        {
            q=aux->ady;
            while(q!=NULL)
            {
                if(q->destino==aux2)
                {
                    if(q==aux->ady)
                        aux->ady=aux->ady->sgte;
                    else
                        r->sgte=q->sgte;
                    delete(q);
                    printf("ARISTA  "<<aux->nombre<<"----->"<<aux2->nombre<<" ELIMINADA.....!!!!");
                    return;
                }
            }
            r=q;
            q=q->sgte;
        }
        aux = aux->sgte;
    }
}
/*                      MOSTRAR GRAFO
    funcion que imprime un grafo en su forma enlazada
---------------------------------------------------------------------*/
void mostrar_grafo()
{   Tnodo ptr;
    Tarista ar;
    ptr=p;
    printf("NODO|LISTA DE ADYACENCIA\n");
 
    while(ptr!=NULL)
    {   printf("   "<<ptr->nombre<<"|");
        if(ptr->ady!=NULL)
        {
            ar=ptr->ady;
            while(ar!=NULL)
            {   printf(" "<<ar->destino->nombre);
                ar=ar->sgte;
             }
 
        }
        ptr=ptr->sgte;
        printf(endl);
    }
}
 
/*                      MOSTRAR ARISTAS
    funcion que muestra todas las aristas de un nodo seleccionado
---------------------------------------------------------------------*/
void mostrar_aristas()
{
    Tnodo aux;
    Tarista ar;
    char var;
    printf(<<"MOSTRAR ARISTAS DE NODO\n");
    printf(<"INGRESE NODO:");
    scanf("%d", var);
    aux=p;
    while(aux!=NULL)
    {
        if(aux->nombre==var)
        {
            if(aux->ady==NULL){   
                printf("EL NODO NO TIENE ARISTAS...!!!!");
                return;
             }
            else
            {
                printf("NODO|LISTA DE ADYACENCIA\n";
               printf("   "<<aux->nombre<<"|");
                ar=aux->ady;
 
                while(ar!=NULL)
                {
                    printf(<<ar->destino->nombre<<" ");
                    ar=ar->sgte;
                }
                printf(endl);
                return;
            }
        }
        else
        aux=aux->sgte;
    }
}



Referencias
// https://codebotic.blogspot.com/2014/06/implementacion-de-un-grafo-dirigido-en.html
