#include<stdio.h>
#include<string.h>
#include<stdlib.h>

typedef struct Carta{
  char *representacion;
  char *nombre;
  char *familia;
  int valor;
  int Valor_real;
  struct Carta *siguiente;
  struct Carta *anterior;

}Carta;


Carta *primera=NULL;
Carta *ultima=NULL;
Carta *anterior=NULL;


int Espadas= 0;
int Corazones= 13;
int Diamantes= 26;
int Treboles= 39;

char Familia_espada[20]= "Espadas";
char Familia_corazones[20]="Corazones";
char Familia_Diamantes[20]="Diamantes";
char Familia_Treboles[20]="Treboles";

void crearBaraja();
void imprimirBaraja();


void crearBaraja(){
	Carta *puntero =(Carta*) malloc(sizeof(Carta));
  	puntero->valor= 2;
	int contador;
	contador= 0;
  	while(contador!=52){
		  if(puntero->valor == 14){
			  puntero->valor = puntero->valor-13;
			}
			Carta *nueva=(Carta*)malloc(sizeof(Carta));
    	if (primera==NULL){

			  nueva->valor= puntero->valor;
      		nueva->Valor_real= nueva->valor+ Espadas;
      		nueva->valor= nueva->valor;
      		nueva->familia= Familia_espada;

			  primera= nueva;
        	ultima= nueva;
        	anterior= nueva;
        	primera->siguiente= NULL;
        	
        	contador= contador+1;
        	puntero->valor= puntero->valor;

    		}else{
          contador= contador+1;
				  puntero->valor= puntero->valor+1;
      			if(contador<=52){

        		  nueva->valor= puntero->valor;
        		  nueva->Valor_real= nueva->valor+ Treboles;
        		  nueva->familia= Familia_Treboles;
        		  nueva->valor= nueva->valor;

        		  ultima->siguiente= nueva;
        		  nueva->siguiente= NULL;
	    				ultima= nueva;
        		  anterior= nueva;
						}

      			if(contador<=39){

        			    nueva->valor= puntero->valor;
        			nueva->Valor_real= nueva->valor+ Diamantes;
        			nueva->familia= Familia_Diamantes;
        			nueva->valor= nueva->valor;

        			ultima->siguiente= nueva;
        			nueva->siguiente= NULL;
	    				ultima= nueva;
        			anterior= nueva;
      			}

      			if(contador<=26){
       	 		  nueva->valor= puntero->valor;
        		  nueva->Valor_real= nueva->valor+ Corazones;
        		  nueva->familia= Familia_corazones;
        		  nueva->valor= nueva->valor;

        		  ultima->siguiente= nueva;
        		  nueva->siguiente= NULL;
	    			  ultima= nueva;
        		  anterior= nueva;
      			}

      			if(contador<=13){

        			nueva->valor= puntero->valor;
        			nueva->Valor_real= nueva->valor+ Espadas;
        			nueva->familia= Familia_espada;
        			nueva->valor= nueva->valor;

        			ultima->siguiente= nueva;
        			nueva->siguiente= NULL;
	    				ultima= nueva;
        			anterior= nueva;
      			}
     		 }
    	}

  }

void imprimirBaraja(){
	int largo;
	largo= 0;
	Carta *temp=(Carta*)malloc(sizeof(Carta));
	temp=primera;
	while(temp->siguiente!=NULL){
		printf("\n   %i De %s",temp->valor, temp->familia);
		temp = temp->siguiente;
  	largo++;
	}
	printf("\n   %i De %s",temp->valor, temp->familia);
  largo++;
}



void Menu(){
	int opcion;
	do{
		printf("\n\n\n\n\n");
		printf("\n---------------------------------------------------------------------");
		printf("\n(0) Mostrar Carta Actual");
		printf("\n(1) Mostrar siguente carta");
		printf("\n(2) Mostrar carta anterior");
		printf("\n(3) Mostrar Toda la baraja");
		printf("\n(4) Ordenar");
		printf("\n(5) Barajar");
		printf("\n(6) Salir");
		printf("\n---------------------------------------------------------------------");
		printf("\n>> Opcion deseada: ");
		scanf("%d",&opcion);
		switch(opcion){
			case 0:
				break;
				
			case 1: 

				break;
			case 2:

				break;
			
			case 3:	
				return imprimirBaraja();
		  	break;
			case 4:
			
				break;
			case 5:
				break;
				
			case 6: 
				 system("PAUSE");  

				break;		
		}
	}while(opcion != 0);
}

int main() {
	crearBaraja();
	Menu();
	return 0;
}
