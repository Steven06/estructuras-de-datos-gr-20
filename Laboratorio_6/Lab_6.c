#include <stdio.h>
#include <stdlib.h>
#include <time.h>

 struct nodoArbol {
   struct nodoArbol *punteroizquierda;
   int dato;
   struct nodoArbol *punteroDerecha;
}; 
typedef struct nodoArbol NodoArbol;
typedef NodoArbol *ptrNodoArbol;
 
/* funciones */
void insertaNodo( ptrNodoArbol *ptrArbol, int valor );
void postOrden( ptrNodoArbol ptrArbol );

/* la función main comienza la ejecución del programa */
int main(){ 
   int i; /* contador para el ciclo de 1 a 10 */
   int elemento; /* variable para almacenar valores al azar */   
   ptrNodoArbol ptrRaiz = NULL; /* árbol inicialemnte vacío */
 
   srand( time( NULL ) ); 
   printf( "Los numeros colocados en el arbol son:\n" );
    /* inserta valores al azar entre 1 y 15 en el árbol */
   for ( i = 1; i <= 10; i++ ) { 
      elemento = rand() % 15;
      printf( "%3d", elemento );
      insertaNodo( &ptrRaiz, elemento);   
   } /* fin del ciclo for */
 
    /* recorre el árbol en postOrden */
   printf( "\nEl recorrido en postOrden es:\n" );
   postOrden(ptrRaiz);
 
   return 0; /* indica terminación exitosa */ 
} /* fin de main */

void insertaNodo( ptrNodoArbol *ptrArbol, int valor ){ 
   
   /* si el árbol esta vacío */
   if ( *ptrArbol == NULL ) {   
      *ptrArbol = malloc( sizeof( NodoArbol ) ); 
      /* si la memoria estaba¡ asignada, entonces asigna el dato */
      if ( *ptrArbol != NULL ) { 
        ( *ptrArbol )->dato = valor;
        ( *ptrArbol )->punteroizquierda = NULL;      
        ( *ptrArbol )->punteroDerecha = NULL;
      } /* fin de if */
      else {
        printf( "no se inserto %d. No hay memoria disponible.\n", valor );
      } /* fin de else */ 
   } /* fin de if */
   else { /* el árbol no esta vacío */
 
      /* el dato a insertar es menor que el dato en el nodo actual */      
      if ( valor < ( *ptrArbol )->dato ) {
        insertaNodo( &( ( *ptrArbol )->punteroizquierda ), valor );
      } /* fin de if */
 
      /* el dato a insertar es mayor que el dato en el nodo actual */      
      else if ( valor > ( *ptrArbol )->dato ) {
        insertaNodo( &( ( *ptrArbol )->punteroDerecha ), valor );
      } /* fin de else if */
      else { /* ignora el valor duplicado del dato */
        printf( "número duplicado" );      
      } /* fin de else */
 
   } /* fin de else */
 
} /* fin de la función insertaNodo */

void postOrden( ptrNodoArbol ptrArbol )
{  
   /* si el árbol no esta vacío, entonces retornar datos.*/
   if ( ptrArbol != NULL ) { 
    postOrden( ptrArbol->punteroizquierda );
    postOrden( ptrArbol->punteroDerecha );      
    printf( "%3d", ptrArbol->dato );
   } /* fin de if */
 
} /* fin de la función postOrden */


/*Referencia http://ejemplos.mis-algoritmos.com/crea-un-arbol-binario-y-lo-recorre-en-preorden-inorden-y-en-postorden*/