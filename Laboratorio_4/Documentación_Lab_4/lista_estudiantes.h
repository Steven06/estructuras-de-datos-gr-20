 /**********************************************************************
    Instituto Tecnológico de Costa Rica
    Estructuras de Datos IC-2001
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol
    Ejemplos Prácticos: Lista de Estudiantes
    Estudiante:
**********************************************************************/
//Definición de Macros
#define LONGITUD_MAXIMA_NOMBRE 50 
#define LONGITUD_MAXIMA_CARNET 12

//Definición de Estructuras
typedef struct nodo_estudiante
{
	int carnet;
	char *nombre;

	struct nodo_estudiante *ref_siguiente;
}nodo_estudiante;

typedef struct lista_estudiantes
{
	nodo_estudiante *ref_inicio;
	int cantidad;
}lista_estudiantes;

//Definición de Funciones
/*-----------------------------------------------------------------------
	crear_nodo
	Entradas: No recibe parámetros
	Salidas: Retorna un puntero de tipo nodo_estudiante al nodo creado
	Funcionamiento: 
		- Solicita al usuario ingresar Nombre y Carnet.
		- Crea un puntero de tipo nodo_estudiante
		- Le asigna al nodo el nombre y carnet ingresados.
		- El nodo apunta a NULL.
		- retorna el puntero al nuevo nodo.
-----------------------------------------------------------------------*/
nodo_estudiante* crear_nodo();
/*-----------------------------------------------------------------------
	inicializar_lista
	Entradas: No recibe parámetros.
	Salidas: No tiene retorno.
	Funcionamiento: 
  - Crea un espacio en memoria en la lista de estudiantes.
  - Hace que la lista de estudiantes acceda a la cantidad y que sea cero y después aumente.
  - Hace que la lista estudiantes acceda al inicio de la lista y que sea NULL si no se tiene estudiantes.
-----------------------------------------------------------------------*/
void inicializar_lista();
/*-----------------------------------------------------------------------
	insertar_inicio
	Entradas: Tiene la estructura de nodo de estudiantes que tiene los datos para ser agregados y un apuntador a nuevo para que los datos sean agregados.
	Salidas: Un nuevo estudiante de la estructura de estudiantes en el inicio de la lista.
	Funcionamiento: 
  - Hace una pregunta que si el apuntador de ref_lista es Nulo entonces accede a inicializar_lista y hace el proceso de inicializar_lista.
  - Hace que el apuntador nuevo acceda a apuntador de ref_siguiente y que sea igual a el apuntador de ref_lista y que se asigne a apuntador de ref_inicio de la estructura de lista de estudiantes.
  - Después hace que la ref_lista acceda a ref_inicio y que se asigne a nuevo.
  - Por úúltimo hace que ref_lista acceda a cantidad y se le asigne ref_lista accediendo a cantidad + 1 aumentando la cantidad de estudiantes.
-----------------------------------------------------------------------*/
void insertar_inicio(nodo_estudiante* nuevo);
/*-----------------------------------------------------------------------
	insertar_final
	Entradas: Tiene la estructura de nodo de estudiantes que tiene los datos para ser agregados y un apuntador a nuevo para que los datos sean agregados.
	Salidas: Un nuevo estudiante de la estructura de estudiantes al final de la lista.
	Funcionamiento: 
  - Hace una pregunta que si el apuntador de ref_lista es Nulo entonces accede a inicializar_lista y hace el proceso de inicializar_lista.
  - Hace una pregunta que si el ref_lista acceda a la cantidad y la compara si es igual a cero porqué de esa manera se inserta de primero.
  - Si no es de esa manera hace otra pregunta que la estructura nodo_estudiante tenga un apuntador a un temporal creado.
  - Después ese temporal se le hara que asigne memoria.
  -Luego el temporal se le asignará que ref_lista acceda al ref_inicio.
  - Luego hace un ciclo donde si el temporal accede a ref_siguiente y es distinto de NULL. Hara que el temporal se le asigne el temporal accediendo a ref_siguiente y termina hay la función.
  - Luego el temporal accede a ref_siguiente y se le asignará nuevo.
  - Por último ref_lista accedera a cantidad++ esto para que la cantidad aumente.  
-----------------------------------------------------------------------*/
void insertar_final(nodo_estudiante* nuevo);
/*-----------------------------------------------------------------------
	borrar_por_indice
	Entradas: Recibe un dato entero(int) de nombre indice ingresado por el usuario.
	Salidas: Posibles tres salidas la primera es borrar un dato de la lista de estudiantes. Otra es la lista está vacía por no haber ningún elemento. Y el tercero es que el índice ingresado no es correcto por exceder la cantidad de elementos presentes.
	Funcionamiento: 
  - Tiene la estructura nodo_eestudiante y crea un nuevo puntero temporal.
  - Hace que el temporal crea un espacio en memoria.
  - Luego que al temporal se le asigne NULL.
  - Crea una variable tipo entera (int) llamada contador.
   - Después hace un pregunta (if) que ref_lista acceda a ref_inicio y que se si es igual a nulo (NULL).
   - Entonces que imprima en pantalla que la lista está vacía y retorne al menú.
   - Si no (else) se pregunta (if) que si el indice es mayor o igual a ref_lista accediendo a la cantidad.
   - Esto imprime un error ya que el indice ingresado supera la cantidad de elementos de la lista y retorna al menú.
   - Si no (else) es así entonces del nodo_estudiantes se crea un apuntador que se llama temporal. Y hace que al temporal se le asigne memoria para que luego el temporal se le asigne la ref_lista acceda a ref_inicio.
   - Después hace una pregunta (if) que si el indice es igual igual a 0. Haga que ref_lista acceda a ref_inicio y se le asigne el temporal accediendo a ref_siguiente.
   - Para luego el ref_lista acceda a cantidad-- esto para quitar un elemento. Y en la otra linea se hace un free del temporal que es el elemento ingresado por el usuario y es borrado luego hace un retorno al menu.
   - Si no (else) se cumple eso entonces haga un while para hacer un ciclo que es que el temporal acceda a ref_siguiente y que este sea distinto de nulo (NULL). En la otra linea se hace que aumente el contador++. Y en la otra linea haga una pregunta (if) que hace una comparación de que el contador es igual igual al indice que haga lo siguiente. Que el temporal acceda al ref_siguiente y que se le asigne temporal accediendo al ref_siguiente y que este accceda al ref_siguiente. La otra linea es que el ref_lista acceda a la cantidad-- para ir disminuyendo la cantidad de elementos. Y después retorne al menú.
   - Por último esta un temporal que se le asigna el temporal accediendo ref_siguiente. Para segui avanzando por la la lista. Está linea dentro del else del ciclo.
-----------------------------------------------------------------------*/
void borrar_por_indice(int indice);
 /*-----------------------------------------------------------------------
	buscar_por_indice
	Entradas: Un indice de tipo entero (int).
	Salidas: Retorna el temporal que es el indice buscado del elemento.
	Funcionamiento: 
  - De la estructura nodo_estudiante se crea un apuntador llamado temporal. Siguiente línea el temporal se le asigna memoria y luego al temporal se le asigna nulo (NULL).
  - Se pregunta (if) que si ref_lista accede a ref_inicio y si es igual igual a nulo (NULL). O disyunción que ref_lista acceda a cantidad y que sea igual igual a 0.
  Si esto es así Imprima La lista está vacía y luego retorne el temporal.
  - Si esto no es así (else): Se pregunte (if) indice es mayor o igual ref_lista accediendo a cantidad. Entonces imprima que el indice ingresado no es válido porqué excede los elementos de la lista y retorne el temporal.
  - Si eso no es así se crea un entero (int) de contador se le asigna un cero, de la estructura nodo_estudiante se crea un apuntador temporal, luego a el temporal se le va ha asignar memoria para las características de la estructura nodo_estudiante, luego ese temporal se le va asignar ref_lista para que acceda a ref_inicio.
  - Se crea un while para hacer un ciclo en que su función es: que el temporal acceda a ref_siguiente y que sea distinto de nulo (NULL). Dentro del while hay una pregunta (if) si contador es igual igual a indice si es así returne temporal. Luego esto fuera del if el temporal se le asignará que temporal acceda a ref_siguiente después que el contador aumente para seguir con los otros elementos. Fuera del while la función retorna el temporal creado.   
-----------------------------------------------------------------------*/
nodo_estudiante* buscar_por_indice(int indice);

 /*-----------------------------------------------------------------------
	validar_carnets
	Entradas: Le entran un número de carnet (int) y un número ingresado (int).
	Salidas: No presenta ninguna salida al ser void un retorno nulo.
	Funcionamiento:
  - Presenta un pregunta (if) que es si el carnet almacenado es igual igual al carnet ingresado imprima en pantalla Qué el carnet ingresado es correcto. Si esto no es así (else) imprima en pantalla que el carnet ingresado es incorrecto. 
-----------------------------------------------------------------------*/
void validar_carnets(int carnet_almacenado, int carnet_ingresado);
 /*-----------------------------------------------------------------------
	menu
	Entradas: Entrada no tiene.
	Salidas: No tiene salidad al ser un void.
	Funcionamiento: 
  - Crea un entero de opción, indice y carnet. Luego tiene la estructura nodo de estudiante con un nuevo apuntador a temporal. 
  - La la instrucción repetitiva hacer mientras (Do while) 
  dentro de ella se llama a otra función para imprimir llamada Imprimir(). Lugo dentro de ella se hacen varios prints para que el usuario pueda escojer alguna de ellas. Luego se establece un tamaño máximo que se le asigna un 1. Después sigue una instrucción de que la opción va hacer ingresada por el estudiante y que contentra el tamaño máximo de ingreso. 
  - Luego está el switch que permite agilizar la toma de opciones múltiples en este caso. En el primer caso del do se establece un break que finaliza (termina) la ejecución de el programa. En el caso 1 se crea un temporal que se le asigna a la función crear_nodo. Luego hay mismo se escribe la función de insertar_al_final con el argumento de temporal. Para luego terminar la funcionalidad y volver al menú. En el caso 2 se crea un temporal y se le asigna a crear_nodo() y después se escribe la función insertar_al_inicio con el argumento de temporal. Y luego termina su funcionalidad y vuelve al menú. En el caso 3 imprime en pantalla que cuál posición de carnet desea válidar. Ahí mismo hay una variable llamada indice que es lo que el usuario escribió
  con solo un valor de tipo númerico. Y después tiene un temporal que se le asigna la función de buscar_por_indice con el argumento de indice de la línea anterior. En la siguiente línea hay una pregunta (if) que el temporal sea distinto de nulo (NULL) sí esto es verdadero se imprime en pantalla ¿Cuál es el carnet del estudiante en la posición índice? Después a el carnet se le asigna lo que el usuario dígito en núúmero. En la siguiente línea en la función validar_carnets se le insertan los argumentos que temporal acceda a carnet y que se guarde carnet. Después la función se cumple y se imprime un poco vacío y vuelve al menú. En el caso 4 se imprime en pantalla ¿Qué posición de carnet desea borrar?después se crea una variable llamada indice que se le asigna lo que el usuario inserte en valor núúmerico. En la otra línea con la función borrar_por_indice con el argumento indice creado en la línea anterior.Después hace la función y vuelve al menú. En la misma dirección del do se hace un while que dice que si opción es distinta de 0 se haga todo lo anterior.
-----------------------------------------------------------------------*/
void menu();
 /*-----------------------------------------------------------------------
	main
	Entradas: No tiene parámetros.
	Salidas: No retorna nada porqué el main ingresa al menu() que es un void.
	Funcionamiento:
  - se crea un entero (int) llamado main() sin argumentos en la otra línea tiene la función Menu() lo que hace es llamar a esa función después retorna 0. 
-----------------------------------------------------------------------*/
int main();
 /*-----------------------------------------------------------------------
	get_user_input
	Entradas: Recibe los parámetros size_t y max_size
	Salidas: Retorna un bufer de tipo texto o carácteres (char).
	Funcionamiento:
  - Se crea un puntero de tipo char llamado buffer, luego se crea una variable llamada size_t characters. Luego al buffer se le asigna memoria para el texto o carácter (char), después se hace una pregunta (if) si el buffer es igual igual a nulo (NULL) imprima u error ERROR No fue posible reservar memoria para el buffer porqué no se ha creado una lista de elementos en la estructura de estudiantes y después se salga. Luego fuera del if el characters se le asignará getline que hace una lectura sencilla de los parámetros que esten dentro de ella y stdin es un apuntador hacia otro archivo para obtener una lectura de los parámetros. Luego el buffer dentro de sus parámetros tiene strlen que se utiliza para obtener la longitud de una cadena de caracteres y su funcióón es del buffer restarle uno y asignarle un cero, para que luego en la siguiente línea retorne el buffer. 
-----------------------------------------------------------------------*/
char* get_user_input(size_t max_size);
 /*-----------------------------------------------------------------------
	get_user_numerical_input
	Entradas: Recibe los parámetros size_t y max_size
	Salidas: Retorna un numerical_input de tipo entero (int).
	Funcionamiento: 
  - Por último se crea un entero (int )llamado get_user_numerical_input con los argumentos size_t y max_size), en la siguiente línea crea un entero (int) llamado numerical_input que se le asigna atoi que es un conviertor de la porción inicial que contiene el argumento get_user_input(max_size) que el usuario ingresa entonces se convierte a entero (int) por atoi. Luego en la otra línea retorne numerical_input. 
-----------------------------------------------------------------------*/
int get_user_numerical_input(size_t max_size);
