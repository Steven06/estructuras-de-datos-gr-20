class Nodo:
  def __init__(self, valor = None, siguiente = None, posicion = 0):
    self.valor = valor
    self.siguiente = siguiente
    self.posicion = posicion

  def obtenerValor(self):
    return self.valor

  def obtenerSiguiente(self):
    return self.siguiente

  def actualizarValor(self, nuevoValor):
    self.valor = nuevoValor

  def actualizarSiguiente(self, nuevoSiguiente):
    self.siguiente = nuevoSiguiente


class Lista_enlazada:
  def __init__(self):
    self.cabeza = Nodo()
    self.tamannoLista = None


  def agregar_inicio(self, valor):
    temp = Nodo(valor)
    cur = self.cabeza
    while cur.siguiente != None:
      cur = cur.siguiente
    cur.siguiente = temp
    del temp


  def obtenerTamanoLista(self):
    actual = self.cabeza
    contador = 0
    while actual != None:
      contador = contador + 1
      actual = actual.obtenerSiguiente()
    return contador


  def agregar_final(self, valor):
    if not self.cabeza:
      self.cabeza = Nodo(valor = valor)
      return
    temporal = self.cabeza
    while temporal.siguiente:
      temporal = temporal.siguiente
    temporal.siguiente = Nodo(valor = valor)
  

  def imprimirLista(self):
    elementos = []
    nodo = self.cabeza
    while nodo.siguiente != None:
      nodo = nodo.siguiente 
      elementos.append(nodo.valor)
    print(elementos)


  def reverseList(self):
    elementos = []
    current = self.cabeza
    prev = None

    while current :
      #create tmp to point to next
      tmp = current.siguiente
      # set the next to point to previous
      current.siguiente = prev
      # set the previous to point to current
      prev = current
      #set the current to point to tmp
      current = tmp
    self.cabeza = prev
    print(elementos)


  def imprimirIndiceValor(self, valor):
    actual = self.cabeza
    encontrado = False
    while actual != None and not encontrado:
      if actual.obtenerValor() == valor:
        encontrado = True
      else:
        actual = actual.obtenerSiguiente()

    return encontrado


  def limpiarLista(self, valorIng):
    borrarValor = self.cabeza
    anterior = None
    while borrarValor and borrarValor.valor != valorIng:
      anterior = borrarValor
      borrarValor = borrarValor.siguiente
    if anterior is None:
      self.cabeza = borrarValor.siguiente
    elif borrarValor:
      anterior.siguiente = borrarValor.siguiente
      borrarValor.siguiente = None


miLista = Lista_enlazada()
miNodo = Nodo()
miLista.agregar_inicio(valor=int(input("Ingrese el valor del nodo al inicio: ")))
miLista.agregar_inicio(valor=int(input("Ingrese el valor del nodo al inicio: ")))
miLista.agregar_inicio(valor=int(input("Ingrese el valor del nodo al inicio: ")))
miLista.agregar_inicio(valor=int(input("Ingrese el valor del nodo al inicio: ")))
miLista.agregar_inicio(valor=int(input("Ingrese el valor del nodo al inicio: ")))
miLista.agregar_inicio(valor=int(input("Ingrese el valor del nodo al inicio: ")))
miLista.agregar_inicio(valor=int(input("Ingrese el valor del nodo al inicio: ")))
miLista.agregar_final(valor=int(input("Ingrese el valor del nodo al final: ")))
miLista.imprimirLista()
print("\n")
miLista.reverseList()
print("\n")
print(miLista.obtenerTamanoLista())
print(miLista.imprimirIndiceValor(valor=input("Ingrese el valor del nodo para imprimir su indice: ")))
miNodo.actualizarValor(nuevoValor=input("Ingrese el valor nuevo: "))
miNodo.actualizarSiguiente(nuevoSiguiente=input("Ingrese el siguiente ha actualizar: "))
miLista.imprimirLista()
print("\n")
miLista.reverseList()
print(miLista.imprimirIndiceValor(valor=input("Ingrese el valor del nodo para imprimir su indice: ")))
miLista.limpiarLista(valorIng=int(input("Ingrese el valor del nodo a eliminar: ")))
miLista.imprimirLista()
print("\n")
miLista.reverseList()
print(miLista.obtenerTamanoLista())


