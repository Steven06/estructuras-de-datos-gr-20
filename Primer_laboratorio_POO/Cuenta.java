import java.util.Date;

public class Cuenta{
  private int id;
  private double balance;
  private double tasadeinteresanual;
  private Date fechaDeCreacion;

  public Cuenta(){
    id = 0;
    balance = 0;
  }

  public Cuenta(int id, double balance){
    this.id = id;
    this.balance = balance;
  }

  public int getid(){
    return this.id;
  }

  public void setid(int id){
    this.id = id;;
  }

  public double getbalance(){
    return this.balance;
  }

  public double setbalance(double balance){
    this.balance = balance;
  }

  public double gettasaDeInteresAnual(){
    return this.tasaDeInteresAnual;
  }

  public double settasaDeInteresAnual(double tasadeinteresanual){
    this.tasaDeInteresAnual = tasaDeInteresAnual;
  }

  public Date getfechaDeCreacion(){
    Date fechaDeCreacion = new Date();
    return fechaDeCreacion;
  }

  public void setfechaDeCreacion(Date fechaDeCreacion){
    this.fechaDeCreacion = fechaDeCreacion;
  }

  public double obtenerTasaDeInteresMensual(){
    double tasaDeInteresMensual = tasadeinteresanual /12;
    return tasaDeInteresMensual;
  }

  public double calcularInteresMensual(){
    double calcularInteresMensual = balance * tasadeinteresanual;
    return calcularInteresMensual;
  }

  public double retirarDinero(double retirarDinero){
    balance = balance - retirarDinero;
    return retirarDinero;
  }

  public double depositarDinero(double depositarDinero){
    balance = balance + depositarDinero;
    return depositarDinero;
  }
}

