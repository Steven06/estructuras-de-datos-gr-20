//Steven Alvarado Aguilar
//2019044923
#include <stdio.h>
#include <stdlib.h>

struct Estudiante{
  char Nombre_del_estudiante[25];
  char Carnet_del_estudiante[15];
  struct Estudiante *Siguiente;
};

struct Estudiante *primero, *ultimo;

void mostrar_menu();
void agregar_elemento_final();
void agregar_elemento_inicio();
void verificar_elemento_lista();
void mostrar_lista();
int borrar_elemento_especifico();

int main(void){
  char opcion, ch;
  primero = (struct Estudiante *) NULL; //Puntero que apunta al primero del estudiante.
  ultimo = (struct Estudiante *) NULL; //Puntero que apunta al último del estudiante.

  do{
    mostrar_menu();
    opcion = getchar(); //Mediante la función de biblioteca getchar se puede conseguir la entrada de carácteres uno a uno.

    while((ch = getchar()) != EOF && ch !='\n');
    switch (opcion){
      case '1': agregar_elemento_final();
        break;

      case '2': agregar_elemento_inicio();
        break;

      case '3': verificar_elemento_lista();
        break;
      case '4': borrar_elemento_especifico();
        break;
      case '5': mostrar_lista();
        break;
      case '6': exit( 1 );
        default: printf( "Opción no válida\n" );
        break;
      }
    }while (opcion!='6');
    getchar();
    return 0;
  }

void mostrar_menu(){
  printf("\n\n ===== Menu =====");
  printf("\n 1 - Agregar elemento al final de la lista" );
  printf("\n 2 - Agregar elemento al inicio de la lista" );
  printf("\n 3 - Verificar elemento de la lista");
  printf("\n 4 - Borrar elemento" );
  printf("\n 5 - Mostrar elementos" );
  printf("\n 6 - Salir" );
  printf("\n\n Escoge una opcion: ");
  }

/* Con esta función añadimos un elemento al final de la lista */
void agregar_elemento_final(){
  struct Estudiante *Nuevo_Estudiante;

  /* reservamos memoria para el nuevo elemento */
  Nuevo_Estudiante = (struct Estudiante *) malloc (sizeof(struct Estudiante));
  if(Nuevo_Estudiante == NULL){
    printf(" \n No hay memoria disponible");
  }
  printf("\n ===== Nuevo elemento =====");
  printf("\nIngrese su Nombre de estudiante:" );
  gets(Nuevo_Estudiante->Nombre_del_estudiante);
  printf("\nIngrese su Carnet de estudiante:" );
  gets(Nuevo_Estudiante->Carnet_del_estudiante);
     
  //el campo siguiente va a ser NULL por ser el último elemento
  Nuevo_Estudiante->Siguiente = NULL;
     
  //ahora metemos el nuevo elemento en la lista. lo situamos al final de la lista
  //comprobamos si la lista está vacía. si primero==NULL es que no hay ningún elemento en la lista. también vale ultimo==NULL
  if( primero == NULL ){
    printf( "\n Primer elemento" );
    primero = Nuevo_Estudiante;
    ultimo  = Nuevo_Estudiante;
  }else{
    /* el que hasta ahora era el último tiene que apuntar al nuevo */
    ultimo->Siguiente = Nuevo_Estudiante;
    /* hacemos que el nuevo sea ahora el último */
    ultimo = Nuevo_Estudiante;
  }
}

void agregar_elemento_inicio(){
  struct Estudiante *Nuevo_Estudiante;

  //reservamos memoria para el nuevo elemento
  Nuevo_Estudiante = (struct Estudiante *)malloc(sizeof(struct Estudiante));
  if(Nuevo_Estudiante == NULL){
    printf(" \n No hay memoria disponible");
  }
  printf("\n ===== Nuevo Estudiante =====");
  printf("\nIngrese su Nombre de estudiante:");
  gets(Nuevo_Estudiante->Nombre_del_estudiante);
  printf("\nIngrese su Carnet de estudiante:");
  gets(Nuevo_Estudiante->Carnet_del_estudiante);

  //el campo siguiente va a ser NULL por ser el último elemento
  Nuevo_Estudiante->Siguiente = NULL;

  //ahora metemos el nuevo elemento en la lista. lo situamos al inicio de la lista
  //comprobamos si la lista está vacía. si primero==NULL es que no hay ningún elemento en la lista*/
  if(primero == NULL){
    printf("\n Primer elemento");
    primero = Nuevo_Estudiante;
    ultimo = Nuevo_Estudiante;
  }else{
    /* el que hasta ahora era el primero tiene que apuntar al nuevo */
    Nuevo_Estudiante->Siguiente = primero;
    /* hacemos que el nuevo sea ahora el primero */
    primero = Nuevo_Estudiante;
  }
}


//funcion que elimina el estudiante del ultimo de la lista
int eliminarultimo(struct Estudiante *anterioraeliminar){
	printf("Se ejecuta la funcion eliminarultimo\n");

	struct Estudiante *punteroeliminado = punteroalista->ultimo;
	punteroalista->ultimo = anterioraeliminar;
	anterioraeliminar->Siguiente = NULL;
	free(punteroeliminado);
	printf("Se ha eliminado el ultimo Estudiante");
	return 0;

}

//funcion que elimina el Estudiante al primero de la lista
int eliminarPrincipio(){
	printf("Se ejecuta la funcion eliminarPrincipio\n");
	struct Estudiante *punteroaprincipio = punteroalista->primero;
	if(punteroaprincipio->sig == NULL){
		punteroalista->primero = NULL;
		punteroalista->final = NULL;
		free(punteroaprincipio);
		punteroalista = NULL;
		printf("Se elimino el unico Estudiante de la lista");

	}else{
		punteroalista->primero = punteroaprincipio->Siguiente;
		free(punteroaprincipio);
	}
	return 0;
}

//funcion que elimina Estudiante de la lista, esta llama a eliminarPrincipio y eliminarultimo
int  eliminarEstudiante(int indice){
  struct Estudiante *punteroalista;
	if(punteroalista == NULL){
		printf("La lista esta vacia\n");
		return 0;
	}
	if(indice == 0){
		eliminarPrincipio();
		return 0;
	}else{
		struct Estudiante *punteroindice;
    punteroindice = punteroalista->primero;
		int i = 0;
		while(i != indice-1){
      punteroindice = punteroindice->Siguiente;
      i++;
		}
		struct Estudiante *punteroaeliminar = punteroindice->Siguiente;
		if(punteroaeliminar->Siguiente == NULL){
			eliminarultimo(punteroindice);
			return 0;
		}else{
			punteroindice->Siguiente = punteroaeliminar->Siguiente;
			punteroaeliminar->Siguiente = NULL;
			free(punteroaeliminar);
			printf("Se ha eliminado un estudiante\n");
		}
	}
}


void verificar_elemento_lista(){
  struct Estudiante *Verificar_Estudiante;//lo usamos para recorrer la lista */
  int indice;
  char carnet_buscado[15];
  printf("Cual es la posicion del carnet que desea validar?\n");
  scanf("%d", &indice);
  printf("Digite el carnet de esa posición: \n");
  scanf("%s", &carnet_buscado[15]);
  if(Verificar_Estudiante == NULL){
    printf("La lista esta vacia\n");
  }else{
    struct Estudiante *Verificar_indice;
    Verificar_indice = Verificar_Estudiante->primero;
    int i = 0;
    while(i != indice){
			Verificar_indice = Verificar_indice->Siguiente;
			i++;
		}
    if(carnet_buscado == Verificar_indice->Carnet_del_estudiante){
			printf("El carnet  ingresado es correcto\n");
    }else{
        printf("El carnet ingresado no es correcto\n");
    }
  }
}


void mostrar_lista(){
  struct Estudiante *auxiliar; /* lo usamos para recorrer la lista */
  int indice = 0;    
  auxiliar = primero;
  printf("\n Mostrando la lista completa:\n ");
  while(auxiliar != NULL){
    printf("\nNombre del estudiante: %s, Carnét del estudiante: %s", auxiliar->Nombre_del_estudiante, auxiliar->Carnet_del_estudiante);
    auxiliar = auxiliar->Siguiente;
    indice++;
  }
  if(indice == 0){
    printf("\n La lista esta vacia!!\n");
  } 
  getchar();
}


/*Referencias
https://github.com/ProgramacionExplciada/Estructura-de-datos-en-C/blob/master/Estructura%20de%20datos%20en%20C%20-%20Lista%20Simple%20-%20Parte%204%20-%20Eliminar%20Nodo.c
https://hackxcrack.net/foro/c/lista-enlazada-simple-borrar-nodo-(lenguaje-c)/
Y la ayuda de Joseph Valenciano/*


