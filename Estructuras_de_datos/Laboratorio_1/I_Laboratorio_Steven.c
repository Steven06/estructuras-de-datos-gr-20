//Steven Alvarado Aguilar.
//Carnét 2019044923
#include <stdio.h>

struct Estudiante{
    int Carnet_estudiante;
    char Nombre_estudiante[25];
}Estudiantes[10];

int main(){
    
    int contador;
    //Hacemos un for para recorrer toda la lista de estudiantes. Y lo guardamos
    //en la variable contador
    for (contador = 0; contador < 10; contador++){
        
        //Limpia la memoria de nuestro buffer.
        fflush(stdin);
        
        printf ("Ingrese el nombre del estudiante%d?\n", contador); //Se guarda en contador porqué cada ciclo que pasa ingresa un dato nievo sin borrar el otro.	
        scanf ("%s", &Estudiantes[contador].Nombre_estudiante); // Ingresamos el nombre y se guarda en nombre de estudiantes.
        printf ("Ingrese el Carnet del estudiante%d?\n", contador); 
        scanf ("%d", &Estudiantes[contador].Carnet_estudiante); // Ingresamos el carnét y se guarda en carnét de estudiantes.
    }
    //Almacena la posición del estudiante que se desea validar.
    int posicion_carnet;
    //Almacena el carnét ingresado del estudiante.
    int numero_carnet_posicion;
    
    printf("¿Qué posición de carnet desea validar?\n");
    scanf("%d", &posicion_carnet);
    
    printf("¿Cuál es el carnet del estudiante en la posición solicitada?\n",posicion_carnet); //Se pone posicion_carnet ya que verificaremos si es el carnet correspondiente a esa posicion.
    scanf("%d", &numero_carnet_posicion);

    if (numero_carnet_posicion != Estudiantes[posicion_carnet].Carnet_estudiante){ //Acá ingresamos especifícamente a la posición del carnet para verificar si es el carnét o no es. 
        printf ("El carnet para el estudiante en posicion:%d \n", posicion_carnet); //Acá verificaremos si es el carnet correspondiente comparandolo.
        printf ("Es %d\n", Estudiantes[posicion_carnet].Carnet_estudiante);
            
    }else{
            printf("El carnet ingresado el correcto");
    }    
    
    return 0;
}

// Obtuve ayuda de las siguientes páginas:
//https://www.fing.edu.uy/tecnoinf/mvd/cursos/prinprog/material/teo/prinprog-teorico08.pdf
//http://www.carlospes.com/curso_de_lenguaje_c/01_08_05_la_funcion_strcpy.php
//http://programandoenc.over-blog.es/article-estructuras-de-registros-44306119.html
// Además de una ayuda de explicación para los últimos reglones del código de Wilhelm Carstens.