//Steven Alvarado Aguilar.
//Carnét 2019044923
#include <stdio.h>

int main(){
    int almacenar_lista;//Acá almacenamos la lista de la lista de estudiantes//
    //una por una sin perder ningún dato.//
    int lista_estudiantes;//Creamos está lista la cuál el usuario va 
    //ingresar la cantidad de estudiantes que desee.
    printf("Digite el número de estudiantes que desea guardar.\n");
    scanf("%d", &lista_estudiantes);
    
    almacenar_lista = lista_estudiantes; //Aquí es donde el almcanar la lista va
    //a tener los estudiantes.
    struct Estudiante{
    int Carnet_estudiante;
    char Nombre_estudiante[25];
    };

    struct Estudiante Estudiantes[almacenar_lista];//La estructura de los 
    //estudiantes que tendrá las características de estudiante y la cantidad. 
    
    //Hacemos un for para saber la cantidad de estudiantes en la lista.
    //En la variable contador se compara con lo que tiene almacenar_lista para 
    //saber cuándo se detiene.
    int contador;
        for (contador = 0; contador < almacenar_lista; contador++){
        
            //Limpia la memoria de nuestro buffer.
            fflush(stdin);
        
            printf ("%d", contador); //Se guarda en contador porqué cada ciclo
            //que pasa ingresa un dato nuevo sin borrar el otro.	
            printf("Escriba el nombre del estudiante?\n");
            scanf("%s", &Estudiantes[contador].Nombre_estudiante);// Ingresamos 
            //el nombre y se guarda en contador en Nombre_estudiante.
            
            printf ("Ingrese el Carnet del estudiante?\n"); 
            scanf("%d", &Estudiantes[contador].Carnet_estudiante); 
            // Ingresamos el carnét y se guarda en contador en Carnet_estudiante.
        }
        
        //Almacena la posición del estudiante que se desea validar.
        int posicion_carnet;
        //Almacena el carnét ingresado del estudiante.
        int numero_carnet_posicion;
    
        printf("¿Qué posición de carnet desea validar?\n");
        scanf("%d", &posicion_carnet);
    
        printf("¿Cuál es el carnet del estudiante en la posición solicitada?\n",posicion_carnet); //Se pone posicion_carnet ya que verificaremos si es 		//el carnet correspondiente a esa posicion.
        scanf("%d", &numero_carnet_posicion);

        if (numero_carnet_posicion != Estudiantes[posicion_carnet].Carnet_estudiante){ //Acá ingresamos especifícamente a la posición del carnet para 		//verificar si es el carnét o no es. 
        printf ("El carnet para el estudiante en posicion:%d \n", posicion_carnet); //Acá verificaremos si es el carnet correspondiente comparandolo.
        printf ("Es %d\n", Estudiantes[posicion_carnet].Carnet_estudiante);
            
        }else{
            printf("El carnet ingresado el correcto");
    }    
    
    return 0;
}

//Obtuve ayuda de las siguientes páginas:
//https://www.youtube.com/watch?v=WxoGvBzWuGs
//https://www.youtube.com/watch?v=Bs5tT29w2t4
//https://www.youtube.com/watch?v=5KiKzWn_jMM
//Además de una ayuda de explicación para los reglones de la estructura
//de estudiantes en el código de Ándres López.
