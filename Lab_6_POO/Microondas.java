/**********************************************************************
    Instituto Tecnológico de Costa Rica
    Programación Orientada a Objetos IC-2101
    II Semestre 2019
    Estudiantes: Randall Zumbado y Steven Alvarado.
    Profesora: Samanta Ramijan Carmiol
**********************************************************************/
public class Microondas {
    
    //Variables a utilizar
    private String tiempodeDuracion;
    private String iniciar;
    private String cancelar;
    private String Name="Microondas";

    //Constructor
    public Microondas(){

        
    }

    //Método para obtener el nombre de la clase
    public String getNombre(){

        return Name;

    }

    //Métodos para obtener diferentes acciones de la clase
    public String getTiempodeDuracion() {
        return tiempodeDuracion;
    }

    public String getIniciar() {
        return iniciar;
    }

    public String getCancelar() {
        return cancelar;
    }
    
    //Métodos para asignar diferentes acciones a la clase
    public void setTiempodeDuracion(String tiempodeDuracion) {
        this.tiempodeDuracion = tiempodeDuracion;
    }

    public void setIniciar(String iniciar) {
        this.iniciar = iniciar;
    }

    public void setCancelar(String cancelar) {
        this.cancelar = cancelar;
    }
}