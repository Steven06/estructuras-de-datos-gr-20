/**********************************************************************
    Instituto Tecnológico de Costa Rica
    Programación Orientada a Objetos IC-2101
    II Semestre 2019
    Estudiantes: Randall Zumbado y Steven Alvarado.
    Profesora: Samanta Ramijan Carmiol
**********************************************************************/
public class CoffeeMaker{
    
    //Variables a utilizar
    private String Name="Coffee Maker";

    //Constructor
    public CoffeeMaker(){

    }

    //Método para obtener el nombre de la clase
    public String getNombre(){

        return Name;


    }
}