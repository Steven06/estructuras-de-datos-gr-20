/**********************************************************************
    Instituto Tecnológico de Costa Rica
    Programación Orientada a Objetos IC-2101
    II Semestre 2019
    Estudiantes: Randall Zumbado y Steven Alvarado.
    Profesora: Samanta Ramijan Carmiol
**********************************************************************/
public class Lavadora {
    
    //Variables a Utilizar
    private String suspender;
    private String aguaFria;
    private String aguaTibia;
    private String aguaCaliente;
    private String tiempodeDuracion;
    private String Name="Lavadora";

    //Constructor
    public Lavadora(){


    }

    //Método para obtener el nombre de la clase
    public String getNombre(){

        return Name;


    }

    //Métodos para obtener diferentes acciones de la clase
    public String getSuspender() {
        return suspender;
    }
    
    public String getAguaFria() {
        return aguaFria;
    }

    public String getAguaTibia() {
        return aguaTibia;
    }

    public String getAguaCaliente() {
        return aguaCaliente;
    }

    public String getTiempodeDuracion() {
        return tiempodeDuracion;
    }

    //Métodos para asignar diferentes acciones a la clase
    public void setSuspender(String suspender) {
        this.suspender = suspender;
    }

    public void setAguaFria(String aguaFria) {
        this.aguaFria = aguaFria;
    }

    public void setAguaTibia(String aguaTibia) {
        this.aguaTibia = aguaTibia;
    }

    public void  setAguaCaliente(String aguaCaliente) {
        this.aguaCaliente = aguaCaliente;
    }

    public void  setTiempodeDuracion(String tiempodeDuracion) {
        this.tiempodeDuracion = tiempodeDuracion;
    }
}