/**********************************************************************
    Instituto Tecnológico de Costa Rica
    Programación Orientada a Objetos IC-2101
    II Semestre 2019
    Estudiantes: Randall Zumbado y Steven Alvarado.
    Profesora: Samanta Ramijan Carmiol
**********************************************************************/
public class Main {
    //Método Principal
    public static void main(String[] args){
        
        //Crea Objetos para su utilización
        Controlador control = new Controlador();
        Computadora computadora= new Computadora();
        Lavadora lavadora=new Lavadora();
        
        //Añade las habitaciones a la casa
        control.agregarHabitacion("Cocina");
        control.agregarHabitacion("Sala");
        control.agregarHabitacion("Oficina");
        control.agregarHabitacion("Cuarto de Pilas");
        control.agregarHabitacion("Baño");
        control.agregarHabitacion("Comedor");
        control.agregarHabitacion("Dormitorio");


        //Imprimir pruebas del uso del controlador con los objetos hechos

        System.out.println("    ");
        System.out.println("-------------------------------------------------");
        System.out.println("    ");
        System.out.println(control.getHabitacion(0));
        System.out.println("    ");
        System.out.println("Encender todos los dispositivos de una habitación");
        System.out.println("    ");
        System.out.println(new CoffeeMaker().getNombre()+"........"+control.encendido());
        System.out.println(new Cocina().getNombre()+ "........" +control.encendido());
        System.out.println(new Luces().getNombre()+"........"+ control.encendido());
        System.out.println(new Microondas().getNombre()+"........"+ control.encendido());
        System.out.println(new Refrigeradora().getNombre()+"........"+ control.encendido());

        System.out.println("    ");
        System.out.println("-------------------------------------------------");
        System.out.println("    ");
        System.out.println(control.getHabitacion(1));
        System.out.println("    ");
        System.out.println("Apagar todos los dispositivos de una habitación");
        System.out.println("    ");
        System.out.println(new DVD().getNombre()+"........"+ control.apagado());
        System.out.println(new Luces().getNombre()+"........"+ control.apagado());
        System.out.println(new Televisor().getNombre()+"........"+ control.apagado());

        System.out.println("    ");
        System.out.println("-------------------------------------------------");
        System.out.println("    ");
        System.out.println(control.getHabitacion(3));
        System.out.println("    ");
        System.out.println("Dispositivos que realizan específicas acciones");
        System.out.println("    ");
        System.out.println(new Lavadora().getNombre()+"........"+ control.encendido());
        System.out.println("    ");
        System.out.println("Tipo de agua");
        lavadora.setAguaCaliente("Agua Caliente");
        System.out.println(lavadora.getAguaCaliente());
        System.out.println("   ");
        lavadora.setTiempodeDuracion("20 minutos");
        System.out.println("Duración de Lavado");
        System.out.println(lavadora.getTiempodeDuracion());

        System.out.println("    ");
        System.out.println("-------------------------------------------------");
        System.out.println("    ");
        System.out.println(control.getHabitacion(2));
        System.out.println("    ");
        System.out.println("Dispositivos que realizan específicas cosas");
        System.out.println("    ");
        System.out.println(new Computadora().getNombre()+"........"+ control.encendido());
        System.out.println("    ");
        System.out.println("Estado de la computadora");
        computadora.setBloquear("Computadora Bloqueada");
        System.out.println(computadora.getBloquear());

        System.out.println("    ");
        System.out.println("-------------------------------------------------");
        System.out.println("    ");
        System.out.println(control.getHabitacion(0));
        System.out.println("    ");
        System.out.println("Dispositivos Apagados o Encendidos en una misma habitación");
        System.out.println("    ");
        System.out.println(new CoffeeMaker().getNombre()+"........"+control.apagado());
        System.out.println(new Cocina().getNombre()+ "........" +control.encendido());
        System.out.println(new Luces().getNombre()+"........"+ control.encendido());
        System.out.println(new Microondas().getNombre()+"........"+ control.apagado());
        System.out.println(new Refrigeradora().getNombre()+"........"+ control.apagado());
    
        

        System.out.println("    ");
        System.out.println("-------------------------------------------------");
        System.out.println("    ");
        System.out.println("CASA");
        System.out.println(control.getHabitaciones());
        System.out.println("    ");
        System.out.println("Encender todas las habitaciones con sus dispositivos");
        System.out.println("    ");
        System.out.println(control.getHabitacion(0)+"........"+control.encendido());
        System.out.println(control.getHabitacion(1)+"........"+control.encendido());
        System.out.println(control.getHabitacion(2)+"........"+control.encendido());
        System.out.println(control.getHabitacion(3)+"........"+control.encendido());
        System.out.println(control.getHabitacion(4)+"........"+control.encendido());
        System.out.println(control.getHabitacion(5)+"........"+control.encendido());
        System.out.println(control.getHabitacion(6)+"........"+control.encendido());
        System.out.println("    ");
    }   
    }
