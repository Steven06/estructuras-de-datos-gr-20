/**********************************************************************
    Instituto Tecnológico de Costa Rica
    Programación Orientada a Objetos IC-2101
    II Semestre 2019
    Estudiantes: Randall Zumbado y Steven Alvarado.
    Profesora: Samanta Ramijan Carmiol
**********************************************************************/
public interface OnOff{
    
    //Métodos abstractos de la interfaz
    public abstract String encendido();
    public abstract String apagado();

}